import React from 'react'
import './App.css'

import { Panel, PanelGroup, PanelResizeHandle } from 'react-resizable-panels'

const App = () => {
    return (
        <div style={{ height: '100vh', display: 'flex'}} >
            <PanelGroup direction="horizontal">
                <Panel>
                    <PanelGroup direction="vertical">
                    
                        <Panel minSize={20} >
                            <PanelGroup direction="horizontal">
                            <Panel minSize={20} className='alignCenter'  >
                                <h1>Window-1</h1>
                                <h4>component-1</h4>
                            </Panel>
                            <PanelResizeHandle style={{ width: '2px', background: 'black' }} />
                            <Panel minSize={20} className='alignCenter' >
                                <h1>Window-2</h1>
                                <h4>component-2</h4>
                            </Panel>
                            </PanelGroup>
                        </Panel>

                        <PanelResizeHandle style={{ height: '2px', background: 'black' }} />

                        <Panel minSize={20} className='alignCenter'  >
                            <h1>Window-3</h1>
                            <h4>component-3</h4>
                        </Panel>
                   

                    </PanelGroup>
                </Panel>
            </PanelGroup>
        </div>
    )
}

export default App